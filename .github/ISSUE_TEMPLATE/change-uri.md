---
name: Change URI
about: Create a report to change URIs of texts
title: ''
labels: URI change suggestion
assignees: ''

---

**OLD URI:** Please, paste old URI.
**NEW URI:** Please, write the new URI.
**REASON:** Please, provide reasons why the URI should be corrected.

**NOTE:** Please, paste the *old* URI into the header.
